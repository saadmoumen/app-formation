import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userCount: number;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAll().subscribe(users => this.userCount = users.length);
  }

}
