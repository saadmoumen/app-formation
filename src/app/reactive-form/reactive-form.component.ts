import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  userForm: FormGroup;
  usernameCtrl: FormControl;
  pwdCtrl: FormControl;
  emailCtrl: FormControl;

  constructor(fb: FormBuilder, private userService: UserService) {
    this.usernameCtrl = fb.control('',
      Validators.required);
    this.pwdCtrl = fb.control('',
      Validators.required);
    this.emailCtrl = fb.control('', [Validators.required],[
      control => { 
        return this.userService.isEmailAvailable(control); 
      }
    ]);

    this.userForm = fb.group({
      username: this.usernameCtrl,
      password: this.pwdCtrl,
      email: this.emailCtrl
    });
  }

  ngOnInit() {
  }

}
