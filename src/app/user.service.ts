import { Injectable, EventEmitter } from '@angular/core';
import { User } from './user';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AbstractControl, FormControl } from '@angular/forms';

@Injectable()
export class UserService {
    API_URL = 'http://localhost:3000/users';

    constructor(private http: HttpClient) { }

    add(user: User): Observable<ResponseType> {
        return this.http.post<ResponseType>(this.API_URL, user);
    }

    getAll(): Observable<User[]> {
        return this.http.get<User[]>(this.API_URL);
    }

    isEmailAvailable(control: AbstractControl) {
        const email = control.value;
        return this.getAll().pipe(
            map(users => users.find(user => user.email === email)),
            map(user => (user) ? null : { alreadyUsed: true })
        );
    }
    delete(id: string): Observable<User> {
      return  this.http.delete<User>(`${this.API_URL}/${id}`);
    }
}
