import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../user';
import { UserService } from '../../user.service';
import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @Input() users: User[];

  constructor(private userService: UserService) {}

  ngOnInit() {}

  delete(id: string) {
    this.userService.delete(id).pipe(
      switchMap(() => this.userService.getAll())
    ).
      subscribe(result => this.users = result);
  }

}
