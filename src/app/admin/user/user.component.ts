import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() user: User;
  @Output() deletedUser: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  deleteUser(): void {
    this.deletedUser.emit(this.user.id);
  }

}
