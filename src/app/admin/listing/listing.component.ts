import { Component, OnInit } from '@angular/core';
import { User } from '../../user';
import { UserService } from '../../user.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  isAdded: boolean;
  isErrored: boolean;
  users: User[];
  myDate = '20181031 00:00:00';

  constructor(private userService: UserService) {
    this.users = [];
  }

  ngOnInit() {
    this.getUsers();
  }

  checkUser(user: User) {
    if (user.firstName && user.lastName && this.users.length < 10) {
      this.userService.add(user)
      .pipe(
        switchMap(() => this.userService.getAll())
      )
      .subscribe((response) => this.users = response, (error) => console.log(error));
      this.isAdded = true;
      this.isErrored = false;
    } else {
      this.isErrored = true;
      this.isAdded = false;
    }
  }

  getUsers() {
    this.userService.getAll().subscribe((response) => this.users = response);
  }

}
