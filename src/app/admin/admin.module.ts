import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UsersComponent } from './users/users.component';
import { FormUserComponent } from './form-user/form-user.component';
import { AlertComponent } from './alert/alert.component';
import { ListingComponent } from './listing/listing.component';
import { RouterModule } from '@angular/router';
import { FromNowPipe } from './from-now.pipe';
import { FormsModule } from '@angular/forms';
import { SimpleFormComponent } from './simple-form/simple-form.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'admin', component: ListingComponent },
      { path: 'form', component: SimpleFormComponent, data: [ {title: 'Formulaire'} ]}
    ]),
    CommonModule,
    FormsModule
  ],
  declarations: [
    UserComponent,
    UsersComponent,
    FormUserComponent,
    AlertComponent,
    FromNowPipe,
    ListingComponent,
    SimpleFormComponent
  ],
  exports: [
    RouterModule
  ]
})
export class AdminModule { }
