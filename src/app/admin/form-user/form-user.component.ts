import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from '../../user';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {
  @Output() userSubmitted = new EventEmitter<User>();
  user: User = {firstName:"" , lastName:""};

  constructor() {}

  ngOnInit() {
  }

  addUser(form: FormGroup) {
    this.userSubmitted.emit({ ...this.user });
    //form.reset();
  }

  /** reset() {
    this.user.lastName = '';
    this.user.firstName = '';
  } */
}
