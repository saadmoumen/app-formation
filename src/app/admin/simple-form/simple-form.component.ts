import { Component, OnInit } from '@angular/core';
import { User } from '../../user';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.scss']
})
export class SimpleFormComponent implements OnInit {
  user: User = {firstName: '', lastName: ''};

  constructor() { }

  ngOnInit() {
  }

  send() {
    console.log('user : ' + JSON.stringify(this.user));
  }

}
